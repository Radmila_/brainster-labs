<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta property="og:title" content="Brainster Lab">
    <meta property="og:image" content="assets/Images/web_thumbnail.png">
    <meta property="og:description" content="Brainster Labs">
    <meta property="og:url" content="http://www.projectsample.tutorialshub.mk/">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/project_style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Brainster Labs</title>
</head>

<body class="firstPage">
    <nav id="navbar" class="navbar navbar-default navbar-fixed-top bg-yellow">
        <div class="navigation container-fluid">
            <div onclick="showSidebar()" class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="logo" class="navbar-brand navbar-left white" href="http://projectsample.tutorialshub.mk"><img src="assets/Images/najnajnajmalo.png"></a>
            </div>
            <div class="sidebar">
                <div>
                    <ul onclick="closeSidebar()" class="nav navbar-nav right-side white-text"><i class="fa fa-times"></i>
                        <li><a href="https://www.brainster.io/marketpreneurs" target="_blank">Академија за маркетинг</a></li>
                        <li><a href="http://codepreneurs.co/" target="_blank">Академија за програмирање</a></li>
                        <li><a href="akademijaZaDataScience.html" target="_blank">Академија за data science</a></li>
                        <li><a href="https://www.brainster.io/design" target="_blank">Академија за дизајн</a></li>
                        <li class="li-btn"><a role="button" class="bg-red btn-nav btn-red" href="http://projectsample.tutorialshub.mk/vrabotistudent.php">Вработи наш студент
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div class="container-fluid hallBackground">
        <div class="row">
            <div class="col-md-12">
                <h1 class="headlines1">Brainster Labs</h1>
            </div>
        </div>
    </div>

    <div class="container-fluid card-container">
        <div class="row threeTiles">
            <div onclick="setActive('marketing')" class="col-md-4 tile">
                <h4>Проекти на студенти од академијата
                    <br> за маркетинг</h4>
                <span class="glyphicon glyphicon-ok glyphicon-white"></span>
            </div>
            <div onclick="setActive('development')" class="col-md-4 tile">
                <h4>Проекти на студенти од академијата
                    <br> за програмирање</h4>
                <span class="glyphicon glyphicon-ok glyphicon-white"></span>
            </div>
            <div onclick="setActive('design')" class="col-md-4 tile">
                <h4>Проекти на студенти од академијата
                    <br> за дизајн</h4>
                <span class="glyphicon glyphicon-ok glyphicon-white"></span>
            </div>
        </div>

        <div class="row bg-yellow">
            <div class="col-md-12 ">
                <h2 class="headlines2">Проекти</h2>
            </div>
        </div>

        <div class="row bg-yellow">
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <?php
                    class Card
                    {
                        public $projectImage;
                        public $projectThumbnail;
                        public $projectTitle;
                        public $projectName;
                        public $projectDescription;
                        public $projectDate;
                        public $projectButton;
                        public $field;
                    }

                    $card1 = new Card();
                    $card1->projectThumbnail = 'https://image.shutterstock.com/image-photo/developing-programming-coding-technologies-website-600w-693112105.jpg';
                    $card1->projectTitle = 'Академија за програмирање';
                    $card1->projectName = 'Име на проектот стои овде во две линии';
                    $card1->projectDescription = 'Краток опис во кој студентите ќе можат да опишат за што се работи во проектот.';
                    $card1->projectDate = 'Април - Октомври 2019';
                    $card1->projectButton = 'Дознај повеќе';
                    $card1->field = 'development';

                    $card2 = new Card();
                    $card2->projectThumbnail = 'https://image.shutterstock.com/image-photo/startup-business-people-group-working-600w-416503771.jpg';
                    $card2->projectTitle = 'Академија за маркетинг';
                    $card2->projectName = 'Име на проектот стои овде во две линии';
                    $card2->projectDescription = 'Краток опис во кој студентите ќе можат да опишат за што се работи во проектот.';
                    $card2->projectDate = 'Април - Октомври 2019';
                    $card2->projectButton = 'Дознај повеќе';
                    $card2->field = 'marketing';

                    $card3 = new Card();
                    $card3->projectThumbnail = 'https://cdn.stocksnap.io/img-thumbs/960w/Y2P39NGLLO.jpg';
                    $card3->projectTitle = 'Академија за дизајн';
                    $card3->projectName = 'Име на проектот стои овде во две линии';
                    $card3->projectDescription = 'Краток опис во кој студентите ќе можат да опишат за што се работи во проектот.';
                    $card3->projectDate = 'Април - Октомври 2019';
                    $card3->projectButton = 'Дознај повеќе';
                    $card3->field = 'design';

                    $cards = array(
                        $card1, $card1, $card1, $card1, $card1, $card1, $card1, $card1, $card1, $card1,
                        $card2, $card2, $card2, $card2, $card2, $card2, $card3, $card3, $card3, $card3
                    );

                    foreach ($cards as $card) {

                        echo '  <div class="col-md-4 col-sm-6 card-holder ' . $card->field . '">
                            <div class="card">
                                <img class="card-img-top" src=" ' . $card->projectThumbnail . '">
                                <div class="cardText">
                                <span class="academyName">' . $card->projectTitle . '</span>
                                    <h5 style="font-weight: bold;">' . $card->projectName . '</h5>
                                    <p>' . $card->projectDescription . '</p>
                                    <h6>' . $card->projectDate . '</h6>
                                    <button class="redButton bg-red pull-right">' . $card->projectButton . '</button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            </div>';
                    }
                    ?>
                </div>

            </div>
        </div>
    </div>

    <div class="row footer">
        <div class="col-md-12 ">
            <h5>Made with <span style="font-size:150%; color:red;">&hearts;</span> by Radmila Serafimoska</h5>
        </div>
    </div>

    <script src="main.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</body>

</html>