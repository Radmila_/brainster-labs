<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/project_style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Project One</title>
</head>

<body>
    <nav id="navbar" class="navbar navbar-default navbar-fixed-top bg-yellow">
        <div class="navigation container-fluid">
            <div onclick="showSidebar()" class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="logo" class="navbar-brand navbar-left white" href="http://projectsample.tutorialshub.mk"><img src="assets/Images/najnajnajmalo.png"></a>
            </div>

            <div class="sidebar">
                <div>
                    <ul onclick="closeSidebar()" class="nav navbar-nav right-side white-text"><i class="fa fa-times"></i>
                        <li><a href="https://www.brainster.io/marketpreneurs" target="_blank">Академија за маркетинг</a></li>
                        <li><a href="http://codepreneurs.co/" target="_blank">Академија за програмирање</a></li>
                        <li><a href="akademijaZaDataScience.html" target="_blank">Академија за data science</a></li>
                        <li><a href="https://www.brainster.io/design" target="_blank">Академија за дизајн</a></li>
                        <li class="li-btn"><a role="button" class="bg-red btn-nav btn-red" href="http://projectsample.tutorialshub.mk/vrabotistudent.php">Вработи наш студент
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- <div class="clearfix"></div> -->

    <div class="container-fluid bg-yellow">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <h1 class="headline3">Вработи студенти</h1>
            </div>
        </div>

        <div class="row formArea">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <form action="" method="POST">
                    <div class="row form-group">
                        <div class="col-md-5 col-md-offset-1 col-sm-12 col-xs-12">
                            <label for="inputName">Име и презиме</label><br>
                            <input type="text" class="form-control inputBox" id="inputName" name="nameAndSurname" placeholder="Вашето име и презиме">
                        </div>

                        <div class="col-md-5 col-sm-12 col-xs-12">
                            <label for="inputCompanyName">Име на компанија</label><br>
                            <input type="text" class="form-control inputBox" id="inputCompanyName" name="company" placeholder="Име на вашата компанија">
                        </div>
                   
                   
                        <div class="col-md-5 col-sm-12 col-md-offset-1 col-xs-12">
                            <label for="inputEmail">Контакт имејл</label><br>
                            <input type="email" class="form-control inputBox" id="inputEmail" name="contactEmail" placeholder="Контакт имејл">
                        </div>

                        <div class="col-md-5 col-sm-12 col-xs-12">
                            <label for="inputTel">Контакт телефон</label><br>
                            <input type="tel" class="form-control inputBox" id="inputTel" name="phoneNumber" placeholder="Контакт телефон на вашата компанија">
                        </div>
                  

                   
                        <div class="col-md-5 col-sm-12 col-md-offset-1 col-xs-12">
                            <label for="inputType">Тип на студенти</label><br>
                            <select class="form-control inputBox" id="inputType"><br>
                                <option type="text" name="typeOfStudent" placeholder="Изберете тип на студент">Изберете тип на студент</option>
                                <option type="text" name="typeOfStudent" value="marketingStudent" placeholder="Изберете тип на студент">Студенти од академијата за маркетинг</option>
                                <option type="text" name="typeOfStudent" value="developmentStudent" placeholder="Изберете тип на студент">Студенти од академијата за програмирање</option>
                                <option type="text" name="typeOfStudent" value="dataScienceStudent" placeholder="Изберете тип на студент">Студенти од академијата за data science</option>
                                <option type="text" name="typeOfStudent" value="designStudent" placeholder="Изберете тип на студент">Студенти од академијата за дизајн</option>

                            </select>
                        </div>

                        <div class="col-md-5 col-sm-12 col-xs-12">
                            <label style="color: #FCD232">Label</label> <br>
                            <input type="submit" class="form-control sendButton inputBox" name="submitButton" value="Испрати">
                        </div>
                    </div>
                </form>
            </div>
        </div>
   
        <div class="clearfix"></div>

    <div class="row footer">
        <div class="col-md-12 col-xs-12">
            <h5>Made with <span style="font-size:150%; color:red;">&hearts;</span> by Radmila Serafimoska</h5>
        </div>
    </div>
    </div>
    <script src="main.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</body>

</html>